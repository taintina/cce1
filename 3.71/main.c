#include <stdio.h>

int main()
{
	int i, error = 0, t = 0, bigletter = 0, noletter = 0, num = 1, endflag = 0;
	char a[10000];
	gets(a);
	for(i = 0; i < strlen(a); i++)
	{
		while(a[i] != ' ')
		{
			t++;
			if(t > 1 && a[i] >= 'A' && a[i] <= 'Z')
			{
				bigletter = 1;
			}
			if(t > 1 && a[i] < 'a' || a[i] > 'z')
			{
				noletter = 1;
			}
			i++;
			if(a[i] == '\0')
			{
				break;
			}
		}

		if(t > 10 || bigletter == 1 || noletter == 1)
		{
			error++;
			bigletter = 0;
			noletter = 0;
		}
		num++;
		t = 0;
	}
	printf("%d error", error);

	return 0;
}