#include <stdio.h>

int main()
{
    int n;
    scanf ("%d", &n);

    printf("%d", fun(n) );

    return 0;
}

int fun(int n)
{
    int ans;
    if(n <= 2)
        ans = 1;
    else
        ans = fun(n-1) +fun(n-2);

    return ans;
}