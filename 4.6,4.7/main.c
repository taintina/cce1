#include <stdio.h>

double fun_ceil(double);
double fun_floor(double);
void main()
{
    double a;
    while(1)
    {
        scanf("%lf", &a);
        printf("math.ceil: %f\tmath.floor: %f\n", ceil(a), floor(a));
        printf("ceil: %f\t\tfloor: %f\n", fun_ceil(a), fun_floor(a));
    }

}

double fun_ceil(double a)
{
    if( (a - (double)(int)a) == 0)
    {
        return a;
    }
    else
    {
        return (double)( (int)(a) + 1 );
    }
}

double fun_floor(double a)
{
    if( (a - (double)(int)a) == 0)
    {
        return a;
    }
    else
    {
        return (double)( (int)(a) );
    }
}
