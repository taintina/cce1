cmake_minimum_required(VERSION 3.15)
project(3_61 C)

set(CMAKE_C_STANDARD 11)

add_executable(3_61 main.c)