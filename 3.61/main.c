#include <stdio.h>

#include <stdio.h>

int main()
{
    float money, ICT = 0, ELC = 0, base = 10000, s;

    printf("entre your salary：\n");
    scanf("%f", &money);
    getchar();

    printf("Are you working in the ICT industry，answer y or n：\n");
    if( getchar() == 'y' )
    {
        ICT = 1;
    }
    else
    {
        ICT = 0;
    }
    getchar();
    printf("Are you collect old electronic equipment for green disposal,answer y or n：\n");
    if( getchar() == 'y' )
    {
        ELC = 1;
        base = 15000;
    }
    else
    {
        ELC = 0;
    }

    if(money <= base)
    {
        s = money * 0.18;
    }
    else if(money > base && money <= base + 8000)
    {
        s = base * 0.18 + (money - base) * 0.2;
    }
    else
    {
        s = base * 0.18 + 8000 * 0.2 + (money - base - 8000) * 0.25;
    }
    s -= s * 0.05 * ICT;

    printf("your tax is %.2f", s);
    return 0;
}
