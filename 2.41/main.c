#include <stdio.h>

int main()
{
    int i, j;
    char name1[10][100], name2[10][100];
    int old[10];
    float money[10], sum = 0;

    for(i = 0; i < 10; i++)
    {
        printf(" %d first name、last name、age、salary：\n", i + 1);
        scanf("%s%s%d%f", name1[i], name2[i], &old[i], &money[i]);
        name1[i][10] = '\0';
        name2[i][10] = '\0';
        sum += money[i];
        printf("%s\t%s\t%d\t%.2f\t\n", name1[i], name2[i], old[i], money[i]);
    }
    printf("average salary %.2f", sum / 10);

    return 0;
}
