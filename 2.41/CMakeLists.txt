cmake_minimum_required(VERSION 3.15)
project(2_41 C)

set(CMAKE_C_STANDARD 11)

add_executable(2_41 main.c)